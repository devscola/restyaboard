# restyaboard: board.devscola.org

Tenemos instalado restyaboard en una maquina de OVH, en `/opt/restyaboard` usando docker y docker-compose.

Tenemos montados un par de volúmenes en `/opt/restyaboard-volumes`, para no perder los ficheros adjuntos y los datos de la base de datos.

El container principal se levanta en el puerto 1234, pero hay un proxy inverso (nginx) que redirige el tráfico desde `board.devscola.org` a ese puerto. Ese dominio está configurado como un CNAME desde la interfaz de usuario de OVH.

El fichero `docker-compose.yml` de este repositorio es una copia exacta del que hay en OVH, quitando las contraseñas e usuarios. Esas solo se pueden ver accediendo a OVH.

El fichero `restyaboard.conf` es la config que hay que meter en `/etc/nginx/sites-available/*` y linkar en `/etc/nginx/sites-enabled/*`:

```
cd /etc/nginx/sites-enabled
ln -s ../sites-available/restyaboard.conf .
nginx -t # test nginx config
nginx -s reload # reload config
```
